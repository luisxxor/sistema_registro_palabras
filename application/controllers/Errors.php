<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends CI_Controller {
  
	public function __construct() {
		parent::__construct();
		$this->load->model('Error_model', 'error');
		$this->load->model('Captioner_model','captioner');
    	$this->load->library('form_validation');
	}
	
	public function index()
	{
		if($this->session->userdata('is_authenticated') == FALSE) {
			http_response_code(403);
			redirect('users/login');
		}

		if($this->input->get('id') == NULL)
		{
			$data['title'] = 'Listado de errores';
			$data['content'] = 'captioners_errors/index';
			$data['vue'] = TRUE;
			$this->load->view('template',$data);
		}
		else
		{
			if($this->input->get('id') != NULL)
			{
				echo json_encode(['captioner_error' => $this->error->find($this->input->get('id'))]);
			}
		}
	}

	function read() {
		if (!$this->session->userdata('is_authenticated')) {
			http_response_code(403);
			echo json_encode(['message' => 'Permission Denied']);
			return null;
		}
		$data['captioners_errors'] = $this->captioner->getOnlyCaptionersWithErrors();
		header('Content-Type: application/json');
		echo json_encode(['captioners_errors' => $data['captioners_errors']]);

	}

	public function getErrorsFromCaptioner() {
		if ($this->session->userdata('is_authenticated') == FALSE) {
			http_response_code(403);
			echo json_encode(['message' => 'Permission Denied']);
			return null;
		}

		$id = $this->input->get('captioner_id');

		$result = $this->error->getAllErrorsFromACaptioner($id);

		if($result > 0)
		{
			echo json_encode(['errors' => $result]);
		}
		else
		{
			echo json_encode(['message' => $result]);
		}
	}
	
	public function create() {
		if ($this->session->userdata('is_authenticated') == FALSE) {
			http_response_code(403);
			echo json_encode(['message' => 'Permission Denied']);
			return null;
		}

		$errors = json_decode($this->input->post('error_form'),true);

		$actual_date = date('Y-m-d');
		$user_id = $this->session->userdata('id');
		$captioner_id = $this->input->post('captioner_id');
		$data = [];

		foreach($errors as $error)
		{
			$error['created_by'] = $user_id;
			$error['created_date'] = $actual_date;
			$error['captioner_id'] = $captioner_id;
			$error['word'] = $error['word'];
			$data[] = $error;
		}
		$ddata = $data;

		$registered_array = $this->errorIsRegistered($data,$captioner_id);

		$is_registered = [];

		foreach($registered_array as $registered)
		{
			$is_registered[] = strtoupper($registered['word']);
		}

		if(count($is_registered) > 0)
		{
			$data = array_filter($data,function($error) use ($is_registered){
				return !in_array(strtoupper($error['word']),$is_registered);
			});
		}

		if(count($data) > 0)
		{
			$result = $this->error->form_insert($data);
		}
		else
		{
			$result = [];
		}

		if ($result != count($data))
		{
			echo json_encode(['failed' => $is_registered, 'success' => [], 'error' => true]);
		}
		else
		{
			echo json_encode(['failed' => $is_registered, 'success' => $data, 'error' => false]);
		}

	}

	public function update() {
		if ($this->session->userdata('is_authenticated') == FALSE) {
			http_response_code(403);
			echo json_encode(['message' => 'Permission Denied']);
			return null;
		}

		$data = json_decode($this->input->post('error_form'),true);

		$result = $this->error->form_update($data);

		if($result > 0)
		{
			echo json_encode(['status' => '200', 'message' => 'Registro actualizado exitosamente']);
		}
		else
		{
			echo json_encode(['status' => '500', 'message' => 'Registro no actualizado, ha ocurrido un problema', 'response' => $result]);
		}
	}

	public function delete() {
		if ($this->session->userdata('is_authenticated') == FALSE) {
			http_response_code(403);
			echo json_encode(['message' => 'Permission Denied']);
			return null;
		} else if($this->session->userdata('is_admin') == FALSE) {
			http_response_code(403);
			echo json_encode(['message' => 'Permission Denied']);
			return null;
		}

		$id = $this->input->post('id');

		$result = $this->error->delete($id);

		if($result > 0)
		{
			echo json_encode(['status' => '200', 'message' => 'Error eliminado correctamente']);
		}
		else
		{
			echo json_encode(['status' => '500', 'message' => 'Error no eliminado, ha ocurrido un problema', 'response' => $result]);
		}
	}

	private function errorIsRegistered($data,$captioner_id) {

		$where_clause = "(";

		foreach($data as $error)
		{
			$where_clause.= "word = ". $this->db->escape($error['word'])." OR ";
		}

		$where_clause = substr($where_clause,0,-4);
		$where_clause.= ');';

		$response = $this->error->errorIsAlreadyRegistered($captioner_id,$where_clause);

		return $response;
	}
}