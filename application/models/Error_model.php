<?php
class Error_model extends CI_Model {

    public function form_insert($data){
        $this->db->insert_batch('errors', $data);
        return $this->db->affected_rows();
    }

    public function form_update($data) {
        $this->db->update_batch('errors',$data,'id');
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('errors');

        return $this->db->affected_rows();
    }
    
    public function getAll() {
        $this->db->select('errors.id,errors.word,CONCAT(captioners.name," ",captioners.lastname) as fullname,errors.created_date,users.username');
        $this->db->from('errors');
        $this->db->join('captioners', 'errors.captioner_id = captioners.id');
        $this->db->join('users', 'errors.created_by = users.id');
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function find($id) {
        $this->db->select('id,word,captioner_id,created_date');
        $this->db->from('errors');
        $this->db->where('id',$id);
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function errorIsAlreadyRegistered($captioner_id,$where_clause) {
        $this->db->select('word');
        $this->db->from('errors');
        $this->db->where('captioner_id',$captioner_id);
        $this->db->where($where_clause);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAllErrorsFromACaptioner($id) {
        $this->db->select('e.id id, e.word word, e.created_date created_date, e.description description');
        $this->db->from('errors e');
        $this->db->join('captioners c', 'e.captioner_id = c.id');
        $this->db->where('c.id',$id);
        $query = $this->db->get();
        return $query->result_array();
    }
}