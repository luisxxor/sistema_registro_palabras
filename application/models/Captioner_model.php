<?php
class Captioner_model extends CI_Model {

    public function form_insert($data) {
        $this->db->insert('captioners', array(
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'rut' => $data['rut'],
        ));
        return $this->db->affected_rows();
    }

    public function form_update($data) {
        $update_data = array(
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'rut' => $data['rut'],
        );

        $this->db->update('captioners',$update_data,array('id' => $data['id']));
        return $this->db->affected_rows();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('captioners');

        return $this->db->affected_rows();
    }
    
    public function getAll() {
        $this->db->select('id,name,lastname,rut');
        $this->db->from('captioners');
        $this->db->order_by('name asc, lastname asc');
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    public function getOnlyCaptionersWithErrors() {
        $this->db->select('c.id,c.name,c.lastname,c.rut');
        $this->db->from('captioners c');
        $this->db->join('errors e', 'c.id = e.captioner_id');
        $this->db->group_by('c.id');
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }
}